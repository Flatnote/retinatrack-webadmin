FROM node:16-alpine3.15 as build
WORKDIR /app
COPY . /app
RUN yarn
RUN yarn build


FROM nginx

RUN rm -f /var/log/nginx/*.log
COPY --from=build /app/build/ /usr/share/nginx/html
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]