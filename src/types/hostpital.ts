export enum HospitalStatus {
	ACTIVE = "Active",
	INACTIVE = "Inactive",
}

export type Hospital = {
	id: number;
	name: string;
	mainColor: string;
	department: string;
	lastUpdate: string;
	status: HospitalStatus;
	image: string;
};
