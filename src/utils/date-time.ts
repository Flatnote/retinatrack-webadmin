export const convertStringToDate = (dateString: string): Date => {
	const [day, month, year] = dateString.split("-");
	return new Date(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(day, 10));
};

export const convertDateToString = (date: Date): string => {
	const day = date.getDate();
	const month = date.getMonth() + 1;
	const year = date.getFullYear();
	return `${day}/${month}/${year}`;
};

export const formatYMDToDMY = (date: string): string => {
	const [year, month, day] = date.split("-");
	return `${day}/${month}/${year}`;
};
