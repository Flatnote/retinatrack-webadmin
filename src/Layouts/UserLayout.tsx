import { faCircleUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FC, useEffect, useRef, useState } from "react";
import { NavLink, Outlet, useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../contexts/Auth";

type UserLayoutProps = {
	children?: React.ReactNode;
};

const mappingHeaderLabel = (pathname: string) => {
	if (pathname.toLowerCase().includes("dashboard")) {
		return "Hospital account";
	}
	if (pathname.toLowerCase().includes("management")) {
		return "Hospital management";
	}
	return "Unknown";
};

const mappingNavigateLabel = (pathname: string) => {
	if (pathname.toLowerCase().includes("dashboard")) {
		return "Dashboard";
	}
	if (pathname.toLowerCase().includes("management")) {
		return "Management";
	}
	return "Unknown";
};

const linkTo = (pathname: string) => {
	if (pathname.toLowerCase().includes("dashboard")) {
		return "/user/dashboard";
	}
	if (pathname.toLowerCase().includes("management")) {
		return "/user/management";
	}
	return "/";
};

const UserLayout: FC<UserLayoutProps> = ({ children }) => {
	const { pathname } = useLocation();
	const navigate = useNavigate();
	const [show, setShow] = useState(false);
	const [isShowUserMenu, setIsShowUserMenu] = useState(false);
	const container = useRef<HTMLDivElement>(null);
	const auth = useAuth();

	const userContainer = useRef<HTMLDivElement>(null);

	useEffect(() => {
		const handleOutsideClick = (event: MouseEvent) => {
			if (!container?.current?.contains(event.target as Node)) {
				if (!show) return;
				setShow(false);
			}
		};
		window.addEventListener("click", handleOutsideClick);
		return () => window.removeEventListener("click", handleOutsideClick);
	}, [show, container]);

	useEffect(() => {
		const handleOutsideClick = (event: MouseEvent) => {
			if (!userContainer?.current?.contains(event.target as Node)) {
				if (!isShowUserMenu) return;
				setIsShowUserMenu(false);
			}
		};
		window.addEventListener("click", handleOutsideClick);
		return () => window.removeEventListener("click", handleOutsideClick);
	}, [isShowUserMenu, userContainer]);

	return (
		<div className="flex h-full w-full flex-col bg-gray-100">
			<div id="nav-bar" className="fixed z-50 flex w-full items-center border-b bg-white shadow">
				<NavLink className="m-3 h-12 w-12 lg:h-16 lg:w-20" to={linkTo(pathname)}>
					<img className="h-full w-full" src="/images/hospital-logo.svg" alt="Logo" />
				</NavLink>
				<div className="grid w-full grid-cols-2 gap-2">
					<div className="col-span-2 flex flex-col-reverse justify-between lg:flex-row">
						<div className="mt-2 ml-2 mr-3 flex items-baseline font-bold text-blue-500">
							Dashboard
							<span className="ml-3 text-sm text-blue-700">{mappingHeaderLabel(pathname)}</span>
						</div>
						<div
							className="my-3 ml-2 mr-3 lg:my-0 lg:ml-0 lg:translate-y-[50%]"
							ref={userContainer}
						>
							<div className="relative">
								<div
									className="flex cursor-pointer items-center justify-end lg:justify-center"
									onClick={() => {
										setIsShowUserMenu(!isShowUserMenu);
									}}
								>
									<FontAwesomeIcon icon={faCircleUser} className="mr-1 mt-1 text-3xl text-black" />
									<div className="ml-1 text-sm text-black">{auth.user}</div>
								</div>

								{isShowUserMenu && (
									<div
										id="user-dropdown"
										className="absolute right-0 z-10 m-2 w-44 divide-y divide-gray-100 rounded bg-white shadow dark:divide-gray-600 dark:bg-gray-700"
									>
										<ul className="py-1 text-sm text-gray-700 dark:text-gray-200">
											<li>
												<div
													className="block cursor-pointer px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
													onClick={() => {
														auth.signout(() => navigate("/"));
													}}
												>
													Sign out
												</div>
											</li>
										</ul>
									</div>
								)}
							</div>
						</div>
					</div>
					<div className="col-span-2 text-sm text-blue-400">
						<div className="relative inline-block text-left" ref={container}>
							<div>
								<button
									type="button"
									className="inline-flex w-full justify-center rounded-md bg-white p-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
									id="menu-button"
									onClick={() => setShow(!show)}
								>
									Pages
									<svg
										className="-mr-1 ml-2 h-5 w-5"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 20 20"
										fill="currentColor"
									>
										<path
											fillRule="evenodd"
											d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
											clipRule="evenodd"
										/>
									</svg>
								</button>
							</div>
							{show && (
								<div className="absolute left-0 mt-2 w-48 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
									<div className="py-1">
										<NavLink
											to="/user/dashboard"
											className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-50"
											id="menu-item-0"
											onClick={() => setShow(false)}
										>
											Dashboard
										</NavLink>
										<NavLink
											to="/user/management"
											className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-50"
											id="menu-item-1"
											onClick={() => setShow(false)}
										>
											Management
										</NavLink>
									</div>
								</div>
							)}
						</div>
						<span className="ml-2 text-black">/</span>
						<NavLink to={linkTo(pathname)} className="ml-2">
							{mappingNavigateLabel(pathname)}
						</NavLink>
					</div>
				</div>
			</div>
			{children}
			<Outlet />
		</div>
	);
};

export default UserLayout;
