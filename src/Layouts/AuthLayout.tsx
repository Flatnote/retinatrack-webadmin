import { FC } from "react";
import { Outlet } from "react-router-dom";
import styled from "styled-components";

type AuthLayoutProps = {
	children?: React.ReactNode;
};

const AuthLayoutBG = styled.div`
	/* background: url("/images/backgrounds/bg_red.jpg") no-repeat center center fixed;
	background-size: cover; */
`;

const AuthLayout: FC<AuthLayoutProps> = () => {
	return (
		<AuthLayoutBG className="h-[100vh]">
			<Outlet />
		</AuthLayoutBG>
	);
};

export default AuthLayout;
