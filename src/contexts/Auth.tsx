import { createContext, useContext } from "react";

interface AuthContextType {
	user: any;
	signin: (user: string, callback: VoidFunction) => void;
	signout: (callback: VoidFunction) => void;
}

export const AuthContext = createContext<AuthContextType>(null!);

export const useAuth = () => {
	return useContext(AuthContext);
};
