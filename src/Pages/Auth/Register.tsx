import { useForm } from "react-hook-form";
import Button from "../../Components/Button";

type FormData = {
	username: string;
	password: string;
};

const Register = () => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<FormData>();

	const onSubmit = (data: FormData) => {
		// console.log("username", username);
		// console.log("password", password);
		// auth.signin(username, () => {
		// 	// Send them back to the page they tried to visit when they were
		// 	// redirected to the login page. Use { replace: true } so we don't create
		// 	// another entry in the history stack for the login page.  This means that
		// 	// when they get to the protected page and click the back button, they
		// 	// won't end up back on the login page, which is also really nice for the
		// 	// user experience.
		// 	navigate(from, { replace: true });
		// });
	};
	return (
		<div className="flex h-full items-center justify-center">
			<div className="mt-4 rounded-2xl bg-white px-8 py-6 text-left shadow-lg">
				<div className="flex justify-center">
					<img src="/images/hospital-logo.svg" alt="Hospital Logo" />
				</div>
				<h3 className="mt-2 text-center text-2xl font-bold text-black">Register new account</h3>
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="mt-4">
						<div className="w-full">
							<label className="block text-black" htmlFor="email">
								Email
							</label>
							<input
								{...register("username", { required: true })}
								type="text"
								placeholder="Email"
								className="mt-2 w-full rounded-md border px-4 py-2 text-black focus:outline-none focus:ring-1 focus:ring-blue-600"
							/>
							{errors.username && errors.username.type === "required" && (
								<p className="mt-1 text-xs text-red-600">Username is required</p>
							)}
						</div>
					</div>
					<div className="mt-4">
						<div className="w-full ">
							<label className="block text-black" htmlFor="password">
								Password
							</label>
							<input
								{...register("password", { required: true })}
								type="password"
								placeholder="Password"
								className="mt-2 w-full rounded-md border px-4 py-2 text-black focus:outline-none focus:ring-1 focus:ring-blue-600"
							/>
							{errors.password && errors.password.type === "required" && (
								<p className="mt-1 text-xs tracking-wide text-red-600">Password is required</p>
							)}
						</div>
					</div>
					<div className="flex items-baseline justify-center">
						<Button className="mt-4 w-full">Register</Button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Register;
