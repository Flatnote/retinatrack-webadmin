import { useForm } from "react-hook-form";
import { Link, useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components";
import Button from "../../Components/Button";
import { useAuth } from "../../contexts/Auth";

type FormData = {
	email: string;
	password: string;
};

const LoginContainer = styled.div`
	/* background: url("/images/backgrounds/login-cover.png") no-repeat center center fixed;
	background-size: cover; */
`;
const LoginCover = styled.div`
	background: url("/images/backgrounds/login-cover.svg") no-repeat right center fixed;
	background-size: cover;
`;

const LoginPage = () => {
	const navigate = useNavigate();
	const location: any = useLocation();
	const auth = useAuth();

	const from: string = location.state?.from?.pathname || "/";

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<FormData>({ mode: "onChange" });

	const onSubmit = (data: FormData) => {
		const { email } = data;

		// console.log("username", username);
		// console.log("password", password);

		auth.signin(email, () => {
			// Send them back to the page they tried to visit when they were
			// redirected to the login page. Use { replace: true } so we don't create
			// another entry in the history stack for the login page.  This means that
			// when they get to the protected page and click the back button, they
			// won't end up back on the login page, which is also really nice for the
			// user experience.
			navigate(from, { replace: true });
		});
	};

	const isEmailValid = (email: string) => {
		return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
	};

	return (
		<LoginContainer className="grid h-full w-full grid-cols-1 overflow-hidden lg:grid-cols-12">
			<div className="z-10 h-full rounded-2xl bg-white shadow-md lg:col-span-4 xl:col-span-4">
				<div className="mt-4 h-5/6 px-8 py-6 pl-10 text-left">
					<div className="">
						<img src="/images/hospital-logo.svg" alt="Hospital Logo" />
					</div>
					<div className="flex h-5/6 flex-col justify-center">
						<h3 className="mt-2 font-light text-black">Welcome to</h3>
						<h3 className="mt-1 text-2xl font-bold text-black">Retina Track Center</h3>
						<form onSubmit={handleSubmit(onSubmit)}>
							<div className="mt-4">
								<div className="w-full">
									<label className="block text-black" htmlFor="email">
										Email
									</label>
									<input
										{...register("email", {
											required: true,
											validate: (value) => isEmailValid(value),
										})}
										type="text"
										placeholder="Email"
										className="mt-2 w-full rounded-md border px-4 py-2 text-black focus:outline-none focus:ring-1 focus:ring-blue-600"
									/>
									{errors.email && errors.email.type === "required" && (
										<p className="mt-1 text-xs text-red-600">Email is required</p>
									)}
									{errors.email && errors.email.type === "validate" && (
										<p className="mt-1 text-xs text-red-600">Email is invalid!</p>
									)}
								</div>
							</div>
							<div className="mt-4">
								<div className="w-full ">
									<label className="block text-black" htmlFor="password">
										Password
									</label>
									<input
										{...register("password", {
											required: true,
										})}
										type="password"
										placeholder="Password"
										className="mt-2 w-full rounded-md border px-4 py-2 text-black focus:outline-none focus:ring-1 focus:ring-blue-600"
									/>
									{errors.password && errors.password.type === "required" && (
										<p className="mt-1 text-xs tracking-wide text-red-600">Password is required</p>
									)}
								</div>
							</div>
							<div className="mt-4 flex items-center justify-between">
								<div className="flex items-center">
									<input
										type="checkbox"
										name="remember-me"
										className="h-4 w-4 rounded text-blue-300"
									/>
									<label className="ml-2 text-sm text-gray-600" htmlFor="remember-me">
										Remember me
									</label>
								</div>
							</div>
							<div className="flex items-baseline justify-center">
								<Button className="mt-4 w-full">Login</Button>
							</div>
							<div className="mt-4 flex justify-center">
								<Link to="/forgotPassword" className="text-sm text-blue-600 hover:underline">
									Forgot password ?
								</Link>
							</div>
						</form>
					</div>
				</div>
			</div>
			<LoginCover className="hidden h-full w-full scale-[1.1] transform-gpu lg:col-span-8 lg:block xl:col-span-8" />
		</LoginContainer>
	);
};

export default LoginPage;
