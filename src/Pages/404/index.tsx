const NotFound = () => {
	return (
		<div className="flex h-[100vh] flex-col items-center justify-center dark:bg-slate-900">
			<h1 className="text-3xl font-bold dark:text-white">404 Not found</h1>
		</div>
	);
};

export default NotFound;
