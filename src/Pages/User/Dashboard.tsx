const UserDashboardPage = () => {
	return (
		<div className="flex h-full items-center justify-center">
			<h1 className="text-black">User Dashboard Page</h1>
		</div>
	);
};

export default UserDashboardPage;
