import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import HospitalItem from "../../Components/HospitalItem";
import { HospitalStatus } from "../../types/hostpital";
import { hospitalHeaders, hospitals } from "../../utils/constants";

type ContainerProps = {
	navBarHeight: number;
};

const Container = styled.div<ContainerProps>`
	width: 100%;
	height: calc(100% - ${(props) => props.navBarHeight}px);
	position: absolute;
	top: ${(props) => `calc(${props.navBarHeight}px)`};
	padding: 2rem;
`;

type TableContainerProps = {
	navBarHeight: number;
};

const TableContainer = styled.div<TableContainerProps>`
	height: calc(100% - ${(props) => props.navBarHeight}px - 1rem);
`;

const HospitalManagement = () => {
	const [navBarHeight, setNavBarHeight] = useState(0);
	const [isShow, setIsShow] = useState(false);
	const dropDownContainer = useRef<HTMLDivElement>(null);

	useEffect(() => {
		const handleResize = () => {
			const navBar = document.getElementById("nav-bar");
			if (navBar) {
				setNavBarHeight(navBar.clientHeight);
			}
		};
		handleResize();
		window.addEventListener("resize", handleResize);
		return () => {
			window.removeEventListener("resize", handleResize);
		};
	}, []);

	useEffect(() => {
		const handleOutsideClick = (event: MouseEvent) => {
			if (!dropDownContainer?.current?.contains(event.target as Node)) {
				if (!isShow) return;
				setIsShow(false);
			}
		};
		window.addEventListener("click", handleOutsideClick);
		return () => window.removeEventListener("click", handleOutsideClick);
	}, [isShow, dropDownContainer]);

	return (
		<Container navBarHeight={navBarHeight}>
			<h1 className="text-lg font-bold text-black">รายชื่อโรงพยาบาลทั้งหมด</h1>
			<div className="ml-auto mr-3" ref={dropDownContainer}>
				<div className="relative inline-block text-left">
					<div className="mt-2 ">
						<button
							type="button"
							className="inline-flex w-full justify-center rounded-md bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100"
							id="menu-button"
							onClick={() => setIsShow(!isShow)}
						>
							All status
							<svg
								className="-mr-1 ml-2 h-5 w-5"
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 20 20"
								fill="currentColor"
							>
								<path
									fillRule="evenodd"
									d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
									clipRule="evenodd"
								/>
							</svg>
						</button>
					</div>
					{isShow && (
						<div className="absolute left-0 mt-2 w-48 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
							<div className="py-1">
								<div
									className="block cursor-pointer px-4 py-2 text-sm text-gray-700 hover:bg-gray-50"
									onClick={() => {
										setIsShow(!isShow);
									}}
								>
									All status
								</div>
								<div
									className="block cursor-pointer px-4 py-2 text-sm text-gray-700 hover:bg-gray-50"
									onClick={() => {
										setIsShow(!isShow);
									}}
								>
									Status 1
								</div>
							</div>
						</div>
					)}
				</div>
			</div>
			<TableContainer
				className="relative mt-8 overflow-x-auto bg-white shadow-md sm:rounded-lg"
				navBarHeight={navBarHeight}
			>
				<table className="w-full text-left text-sm text-gray-500 dark:text-gray-400">
					<thead className=" bg-blue-200 text-sm uppercase text-blue-700 dark:bg-gray-700 dark:text-gray-400">
						<tr>
							{hospitalHeaders.map((header, index) => (
								<th
									scope="col"
									className={`p-6 ${
										index === hospitalHeaders.length - 1 ? "flex items-center justify-end" : ""
									}`}
									key={header}
								>
									{header}
								</th>
							))}
						</tr>
					</thead>
					<tbody>
						{hospitals.map((hospital) => (
							<HospitalItem hospital={hospital} key={hospital.id} />
						))}
					</tbody>
				</table>
				<div className="relative h-[56%] text-sm">
					<div className="absolute bottom-0 left-0 flex w-full justify-between px-5 py-3">
						<div>
							{`Active Hospital: ${
								hospitals.filter((hospital) => hospital.status === HospitalStatus.ACTIVE).length
							} / 
									${hospitals.length}`}
						</div>
						<div className="flex">
							<div className="mr-8">Rows per page: 15</div>
							<div className="mr-8">1-4 of 4</div>
							<div className="mr-5 cursor-pointer hover:bg-gray-50">
								<FontAwesomeIcon icon={faChevronLeft} />
							</div>
							<div className="cursor-pointer hover:bg-gray-50">
								<FontAwesomeIcon icon={faChevronRight} />
							</div>
						</div>
					</div>
				</div>
			</TableContainer>
		</Container>
	);
};

export default HospitalManagement;
