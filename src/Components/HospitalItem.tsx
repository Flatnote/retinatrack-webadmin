import { useState } from "react";
import { Hospital, HospitalStatus } from "../types/hostpital";
import { formatYMDToDMY } from "../utils/date-time";

const HospitalItem = ({ hospital }: { hospital: Hospital }) => {
	const [itemChecked, setItemChecked] = useState(hospital.status === HospitalStatus.ACTIVE);
	const [itemStatus, setItemStatus] = useState(hospital.status);
	return (
		<tr className="border-b bg-white dark:border-gray-700 dark:bg-gray-800">
			<td className="border-t-0 border-b-0 border-l-0 border-r-0">
				<div className="flex items-center px-6 py-4">
					<div className="h-10 w-10 flex-shrink-0">
						<img
							className="h-10 w-10 rounded-full"
							src={hospital.image}
							alt={`Hospital-${hospital.name}`}
						/>
					</div>
					<div className="ml-4">
						<div className="text-sm leading-5 text-gray-900 dark:text-gray-400">
							{hospital.mainColor}
						</div>
						<div className="text-sm font-medium leading-5 text-gray-500 dark:text-gray-300">
							{hospital.name}
						</div>
					</div>
				</div>
			</td>
			<td className="px-6 py-4">{hospital.department}</td>
			<td className="px-6 py-4">{formatYMDToDMY(hospital.lastUpdate)}</td>
			<td className="flex items-center justify-end px-6 py-4">
				<span className="mr-3 text-sm font-medium text-gray-900 dark:text-gray-300">
					{itemStatus}
				</span>
				<label
					htmlFor={`checked-toggle-${hospital.id}`}
					className="relative inline-flex cursor-pointer items-center"
				>
					<input
						type="checkbox"
						value=""
						id={`checked-toggle-${hospital.id}`}
						className="peer sr-only"
						checked={itemChecked}
						onChange={() => {
							setItemChecked(!itemChecked);
							setItemStatus(itemChecked ? HospitalStatus.INACTIVE : HospitalStatus.ACTIVE);
						}}
					/>
					<div className="peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-0.5 after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:ring-4 peer-focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:peer-focus:ring-blue-800"></div>
				</label>
			</td>
		</tr>
	);
};

export default HospitalItem;
