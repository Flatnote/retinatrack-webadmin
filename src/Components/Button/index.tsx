import React from "react";

type ButtonProps = { children: string | JSX.Element; onClick?: VoidFunction; className?: string };

const Button: React.FC<ButtonProps> = ({ children, onClick, className }) => {
	return (
		<button
			className={`${className || ""} rounded-lg bg-blue-600 px-6 py-2 text-white hover:bg-blue-900`}
			onClick={() => {
				if (onClick) {
					onClick();
				}
			}}
		>
			{children}
		</button>
	);
};

export default Button;
