import { Navigate } from "react-router-dom";

const CheckLogin = () => {
	return <Navigate to="/login" />;
};

export default CheckLogin;
