import { useState } from "react";
import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import { fakeAuthProvider } from "./auth";
import { AuthContext, useAuth } from "./contexts/Auth";
import AuthLayout from "./Layouts/AuthLayout";
import UserLayout from "./Layouts/UserLayout";
import NotFound from "./Pages/404";
import LoginPage from "./Pages/Auth/Login";
import UserDashboardPage from "./Pages/User/Dashboard";
import UserManagementPage from "./Pages/User/Management";

const App = () => {
	return (
		<AuthProvider>
			<Routes>
				<Route element={<AuthLayout />}>
					<Route
						path="/"
						element={
							<RequireAuth>
								<Navigate to="/user/dashboard" />
							</RequireAuth>
						}
					/>
					<Route path="/login" element={<LoginPage />} />
					{/* <Route path="/register" element={<Register />} /> */}
					<Route
						path="/user"
						element={
							<RequireAuth>
								<Navigate to="/user/dashboard" />
							</RequireAuth>
						}
					/>
					<Route
						path="/user/dashboard"
						element={
							<RequireAuth>
								<UserLayout>
									<UserDashboardPage />
								</UserLayout>
							</RequireAuth>
						}
					/>
					<Route
						path="/user/management"
						element={
							<RequireAuth>
								<UserLayout>
									<UserManagementPage />
								</UserLayout>
							</RequireAuth>
						}
					/>
					<Route path="*" element={<NotFound />} />
				</Route>
				<Route path="*" element={<NotFound />} />
			</Routes>
		</AuthProvider>
	);
};

const AuthProvider = ({ children }: { children: JSX.Element }) => {
	const [user, setUser] = useState<string | undefined>(undefined);

	const signin = (newUser: string, callback: VoidFunction) => {
		return fakeAuthProvider.signin(() => {
			setUser(newUser);
			callback();
		});
	};

	const signout = (callback: VoidFunction) => {
		return fakeAuthProvider.signout(() => {
			setUser(undefined);
			callback();
		});
	};

	const value = { user, signin, signout };

	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

const RequireAuth = ({ children }: { children: JSX.Element }) => {
	const auth = useAuth();
	const location = useLocation();

	if (!auth.user) {
		// Redirect them to the /login page, but save the current location they were
		// trying to go to when they were redirected. This allows us to send them
		// along to that page after they login, which is a nicer user experience
		// than dropping them off on the home page.
		return <Navigate to="/login" state={{ from: location }} replace />;
	}

	return children;
};

export default App;
